package com.example.mobilecomputinghomework

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import com.example.mobilecomputinghomework.data.repository.CategoryRepository
import com.example.mobilecomputinghomework.data.repository.ReminderRepository
import com.example.mobilecomputinghomework.data.room.MobileComputingHomeworkDatabase

/**
 * A simple singleton dependency graph
 *
 * For a real app something like Koin/Dagget/Hilt should be used
 */
object Graph {
    lateinit var database: MobileComputingHomeworkDatabase
        private set

    lateinit var appContext: Context

    val categoryRepository by lazy {
        CategoryRepository(
            categoryDao = database.categoryDao()
        )
    }

    val reminderRepository by lazy {
        ReminderRepository(
            reminderDao = database.reminderDao()
        )
    }

    fun provide(context: Context) {
        appContext = context
        database = Room.databaseBuilder(context, MobileComputingHomeworkDatabase::class.java, "roomdata.db")
            .fallbackToDestructiveMigration() //should not be used in real app
            .build()
    }
}