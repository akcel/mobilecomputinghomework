package com.example.mobilecomputinghomework.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mobilecomputinghomework.Graph
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.data.repository.CategoryRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class HomeViewModel(
    private val categoryRepository: CategoryRepository = Graph.categoryRepository
) : ViewModel(){
    private val _state = MutableStateFlow(HomeViewState())
    private val _selectedCategory = MutableStateFlow<Category?>(null)

    val state: StateFlow<HomeViewState>
        get() = _state

    fun onCategorySelected(category: Category) {
        _selectedCategory.value = category
    }

    init {
        /*
        val categories = MutableStateFlow<List<Category>>(
            mutableListOf(
                Category(1,"Food"),
                Category(2,"Healt"),
                Category(3,"Savings"),
                Category(4,"Drinks"),
                Category(5,"Clothing"),
                Category(6,"Travel"),
                Category(7,"Investment"),
                Category(8,"Gaming"),
            )
        )*/
        viewModelScope.launch {

            combine(
                categoryRepository.categories().onEach { list ->
                    if (list.isNotEmpty()&& _selectedCategory.value == null) {
                        _selectedCategory.value = list[0]
                    }
                },
                _selectedCategory
            ) { categories, selectedCategory ->
                HomeViewState(
                    categories = categories,
                    selectedCategory = selectedCategory
                )
            }.collect { _state.value = it }
        }
        loadCategoriesFromDB()
    }

    private fun loadCategoriesFromDB() {
        val list = mutableListOf(
            Category(name = "All"),
            Category(name = "Healt"),
            Category(name = "Travel"),
            Category(name = "Investment"),
            Category(name = "Gaming"),
        )
        viewModelScope.launch {
            list.forEach { category -> categoryRepository.addCategory(category)}
        }
    }
}

data class HomeViewState(
    val categories: List<Category> = emptyList(),
    val selectedCategory: Category? = null
)