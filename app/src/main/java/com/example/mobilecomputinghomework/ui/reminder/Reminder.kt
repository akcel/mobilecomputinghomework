package com.example.mobilecomputinghomework.ui.reminder

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.singleton.LocationHolder
import com.example.mobilecomputinghomework.ui.home.categoryReminder.parseToTime
import com.example.mobilecomputinghomework.ui.home.categoryReminder.toDateString
import com.google.accompanist.insets.systemBarsPadding
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch
import java.util.*

@Composable
fun Reminder(
    onBackPress: () -> Unit,
    viewModel: ReminderViewModel = viewModel(),
    navController: NavController
) {
    val viewState by viewModel.state.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    val title = rememberSaveable{ mutableStateOf("") }
    val category = rememberSaveable{ mutableStateOf("") }
    val date = rememberSaveable{ mutableStateOf(Date().time.toDateString()) }

    val latlng = navController
        .currentBackStackEntry
        ?.savedStateHandle
        ?.getLiveData<LatLng>("location_data")
        ?.value
    Surface() {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .systemBarsPadding()
        ) {
            TopAppBar {
                IconButton(
                    onClick = onBackPress
                ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = null
                    )
                }
                Text(text = "New reminder")
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top,
                modifier = Modifier.padding(16.dp),
            ) {
                OutlinedTextField(
                    value = title.value,
                    onValueChange = { title.value = it },
                    label = { Text("Message")},
                    modifier = Modifier.fillMaxWidth(),
                )
                /*Spacer(modifier = Modifier.height(10.dp))
                CategoryListDropdown(
                    viewState = viewState,
                    category = category
                )*/
                /*Spacer(modifier = Modifier.height(10.dp))
                OutlinedTextField(
                    value = amount.value,
                    onValueChange = { amount.value = it },
                    label = { Text("amount")},
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number
                    )

                )*/
                Spacer(modifier = Modifier.height(10.dp))
                OutlinedTextField(
                    value = date.value,
                    onValueChange = { date.value = it },
                    label = { Text("Reminder time")},
                    modifier = Modifier.fillMaxWidth(),
                    //keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                )
                Spacer(modifier = Modifier.height(20.dp))
                if (latlng == null) {
                    OutlinedButton(
                        onClick = { /*TODO*/
                            navController.navigate("map")
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .size(40.dp),
                    ) {
                        Text(text = "Set location")
                    }
                } else {
                    OutlinedTextField(
                        value = "${latlng.latitude}, ${latlng.longitude}",
                        onValueChange = { },
                        label = { Text("Reminder location")},
                        modifier = Modifier
                            .fillMaxWidth(),
                        enabled = false
                    )
                }
                Spacer(modifier = Modifier.height(20.dp))
                Button(
                    onClick = {
                        coroutineScope.launch {
                            viewModel.saveReminder(
                                com.example.mobilecomputinghomework.data.entity.Reminder(
                                    reminderMessage = title.value,
                                    reminderTime = date.value.parseToTime(),
                                    reminderCategoryId = getCategoryId(viewState.categories, "All"),
                                    locationLat = latlng?.latitude,
                                    locationLng = latlng?.longitude
                                )
                            )
                        }
                        onBackPress()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(55.dp),
                ) {
                    Text(text = "Save reminder")
                }
                //TODO POISTA TÄÄ
                Spacer(modifier = Modifier.height(20.dp))
                OutlinedTextField(
                    value = LocationHolder.readLocation().toString(),
                    onValueChange = { },
                    label = { Text("Last Known Location")},
                    modifier = Modifier.fillMaxWidth(),
                    enabled = false,
                ) //TODO TÄHÄN ASTI
            }
        }
    }
}

private fun getCategoryId(categories: List<Category>, categoryName: String): Long {
    return categories.first { category -> category.name == categoryName }.id
}

@Composable
private fun CategoryListDropdown(
    viewState: ReminderViewState,
    category: MutableState<String>
) {
    var expanded by remember { mutableStateOf(false) }
    val icon = if (expanded){
        Icons.Filled.ArrowDropUp //requires androidx.compose.material:material-icons-extended
    } else {
        Icons.Filled.ArrowDropDown
    }

    Column {
        OutlinedTextField(
            value = category.value,
            onValueChange = {category.value = it},
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Category")},
            readOnly = true,
            trailingIcon = {
                Icon(
                    imageVector = icon,
                    contentDescription = null,
                    modifier = Modifier.clickable { expanded = !expanded}
                )
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth()
        ) {
            viewState.categories.forEach { dropDownOption ->
                DropdownMenuItem(
                    onClick = {
                        category.value = dropDownOption.name
                        expanded = false
                    }
                ) {
                    Text(dropDownOption.name)
                }
            }
        }
    }
}