package com.example.mobilecomputinghomework.ui.home.categoryReminder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mobilecomputinghomework.Graph
import com.example.mobilecomputinghomework.data.repository.ReminderRepository
import com.example.mobilecomputinghomework.data.room.ReminderToCategory
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


class CategoryReminderViewModel(
    private val categoryId: Long,
    private val reminderRepository: ReminderRepository = Graph.reminderRepository
) : ViewModel() {
    private val _state = MutableStateFlow(CategoryReminderViewState())
    val state: StateFlow<CategoryReminderViewState>
        get() = _state


    init {
        viewModelScope.launch {
            reminderRepository.remindersInCategory(categoryId).collect { list ->
                _state.value = CategoryReminderViewState(
                    reminders = list
                )
            }
        }
        /*
        val list = mutableListOf<Reminder>()
        for (x in 1..12) {
            list.add(
                Reminder(
                    reminderId = x.toLong(),
                    reminderTitle = "Reminder $x",
                    reminderCategoryId = 1,
                    /*if (x < 5) {
                        "Todo"
                    } else {
                        "Event"
                    },*/
                    reminderDate = 234
                )
            )
        }


        viewModelScope.launch {
            _state.value = CategoryReminderViewState(
                reminders = list
            )
        }*/
    }
}

data class CategoryReminderViewState(
    val reminders: List<ReminderToCategory> = emptyList()
)