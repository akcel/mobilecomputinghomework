package com.example.mobilecomputinghomework.ui.home.categoryReminder

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.mobilecomputinghomework.R
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.data.entity.Reminder
import com.example.mobilecomputinghomework.data.room.ReminderToCategory
import com.example.mobilecomputinghomework.singleton.ReminderHolder
import com.example.mobilecomputinghomework.ui.reminder.ReminderViewModel
import com.example.mobilecomputinghomework.util.viewModelProviderFactoryOf
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun CategoryReminder(
    categoryId: Long,
    modifier: Modifier = Modifier,
    navController: NavController
) {
    val viewModel: CategoryReminderViewModel = viewModel(
        key = "category_list_$categoryId",
        factory = viewModelProviderFactoryOf {CategoryReminderViewModel(categoryId)}
    )
    val viewState by viewModel.state.collectAsState()
    Column(modifier = modifier) {
        ReminderList(
            list = viewState.reminders,
            navController = navController
        )
    }
}

@Composable
private fun ReminderList(
    list: List<ReminderToCategory>,
    navController: NavController,
    viewModel: ReminderViewModel = viewModel(),
) {
    val coroutineScope = rememberCoroutineScope()
    LazyColumn(
        contentPadding = PaddingValues(0.dp),
        verticalArrangement = Arrangement.Center
    ) {
        items(list) { item ->
            if (item.reminder.reminderTime < Date().time) {
                ReminderListItem(
                    reminder = item.reminder,
                    category = item.category,
                    onClick = {/*TODO*/
                        ReminderHolder.writeMessage(item.reminder.reminderMessage)
                        ReminderHolder.writeDate(item.reminder.reminderTime)
                        coroutineScope.launch {
                            viewModel.deleteReminder(item.reminder)
                        }
                        navController.navigate(route = "edit_reminder")
                    },
                    modifier = Modifier.fillParentMaxWidth(),
                )
            }
        }
    }
}

@Composable
private fun ReminderListItem(
    reminder: Reminder,
    category: Category,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    viewModel: ReminderViewModel = viewModel(),
) {
    val coroutineScope = rememberCoroutineScope()

    ConstraintLayout(modifier = modifier.clickable { onClick() }) {
        val (divider, reminderTitle, reminderCategory, icon, date) = createRefs()
        Divider(
            modifier = Modifier.constrainAs(divider) { //TODO
                top.linkTo(parent.top)
                centerHorizontallyTo(parent)
                width = Dimension.fillToConstraints
            }
        )
        // title
        Text(
            text = reminder.reminderMessage,
            maxLines = 1,
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier.constrainAs(reminderTitle){
                linkTo(
                    start = parent.start,
                    end = icon.start,
                    startMargin = 24.dp,
                    endMargin = 16.dp,
                    bias = 0f
                )
                top.linkTo(parent.top, margin = 10.dp)
                width = Dimension.preferredWrapContent
            }
        )
        //category
        /*Text(
            text = category.name,
            maxLines = 1,
            style = MaterialTheme.typography.subtitle2,
            modifier = Modifier.constrainAs(reminderCategory){
                linkTo(
                    start = parent.start,
                    end = icon.start,
                    startMargin = 24.dp,
                    endMargin = 8.dp,
                    bias = 0f
                )
                top.linkTo(reminderTitle.bottom, margin = 6.dp)
                bottom.linkTo(parent.bottom, 10.dp)
                width = Dimension.preferredWrapContent
            }
        )*/
        //date
        Text(
            /*
            text = when {
                reminder.reminderDate != null -> { reminder.reminderDate.formatToString() }
                else -> Date().formatToString()
            },*/
            text = reminder.reminderTime.toDateString(),//.toDateString(), //TODO
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.constrainAs(date) {
                linkTo(
                    start = reminderCategory.end,
                    end = icon.start,
                    startMargin = 8.dp,
                    endMargin = 16.dp,
                    bias = 0f
                )
                centerVerticallyTo(reminderCategory)
                top.linkTo(reminderTitle.bottom, 6.dp)
                bottom.linkTo(parent.bottom, 10.dp)
            }
        )
        //icon
        IconButton(
            onClick ={
                coroutineScope.launch {
                    viewModel.deleteReminder(reminder)
                }
                     },
            modifier = Modifier
                .size(50.dp)
                .padding(6.dp)
                .constrainAs(icon) {
                    top.linkTo(parent.top, 10.dp)
                    bottom.linkTo(parent.bottom, 10.dp)
                    end.linkTo(parent.end)
                }
        ) {
            Icon(
                imageVector = Icons.Filled.Check,
                contentDescription = stringResource(R.string.checkmark)
            )
        }
    }
}

fun Date.formatToString(): String {
    return SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(this)
}

fun Long.toDateString(): String {
    return SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date(this))
}

fun String.parseToTime(): Long {
    return SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(this).time
}