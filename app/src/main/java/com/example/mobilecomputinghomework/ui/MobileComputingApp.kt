package com.example.mobilecomputinghomework.ui

import androidx.compose.runtime.Composable
import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.mobilecomputinghomework.MobileComputingAppState
import com.example.mobilecomputinghomework.data.entity.Reminder
import com.example.mobilecomputinghomework.rememberMobileComputingAppState
import com.example.mobilecomputinghomework.ui.home.Home
import com.example.mobilecomputinghomework.ui.login.Login
import com.example.mobilecomputinghomework.ui.maps.ReminderLocationMap
import com.example.mobilecomputinghomework.ui.reminder.EditReminder
import com.example.mobilecomputinghomework.ui.reminder.Reminder

@Composable
fun MobileComputingApp(
    appState: MobileComputingAppState = rememberMobileComputingAppState()
) {
    NavHost(
        navController = appState.navController,
        startDestination = "login"
    ) {
        composable(route = "login") {
            Login(navController = appState.navController)
        }
        composable(route = "home") {
            Home(navController = appState.navController)
        }
        composable(route = "reminder") {
            Reminder(onBackPress = { appState.navigateBack() }, navController = appState.navController)
        }
        composable(route = "edit_reminder"/*, List<NamedNavArgument>() */) {
            EditReminder(onBackPress = { appState.navigateBack() })
        }
        composable(route = "create_account") {
            // TODO
        }
        composable(route = "map") {
            ReminderLocationMap(navController = appState.navController)
        }

    }
}
