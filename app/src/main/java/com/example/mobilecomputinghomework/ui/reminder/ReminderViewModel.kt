package com.example.mobilecomputinghomework.ui.reminder

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.NotificationManagerCompat.from
import androidx.core.content.getSystemService
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.example.mobilecomputinghomework.Graph
import com.example.mobilecomputinghomework.R
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.data.entity.Reminder
import com.example.mobilecomputinghomework.data.repository.CategoryRepository
import com.example.mobilecomputinghomework.data.repository.ReminderRepository
import com.example.mobilecomputinghomework.ui.home.categoryReminder.toDateString
import com.example.mobilecomputinghomework.util.LocationNotificationWorker
import com.example.mobilecomputinghomework.util.NotificationWorker
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.time.Duration
import java.util.*
import java.util.concurrent.TimeUnit
import android.R.id




class ReminderViewModel (
    private val reminderRepository: ReminderRepository = Graph.reminderRepository,
    private val categoryRepository: CategoryRepository = Graph.categoryRepository,
): ViewModel() {
    private val _state = MutableStateFlow(ReminderViewState())

    val state: StateFlow<ReminderViewState>
        get() = _state

    suspend fun saveReminder(reminder: Reminder): Long {
        if(reminder.locationLat == null) {
            setReminderByTime(reminder)
        } else {
            setReminderByLocation(reminder)
        }

        return reminderRepository.addReminder(reminder)
    }

    suspend fun deleteReminder(reminder: Reminder): Int {
        return reminderRepository.deleteReminder(reminder)
    }

    init {
        createNotificationChannel(context = Graph.appContext)
        //setOneTimeDelayedNotification()
        viewModelScope.launch {
            categoryRepository.categories().collect { categories ->
                _state.value = ReminderViewState(categories)
            }
        }
    }
}

private fun setOneTimeDelayedNotification() {
    val workManager = WorkManager.getInstance(Graph.appContext)
    val constraints = Constraints.Builder()
        .setRequiresBatteryNotLow(true)
        .build()

    val notificationWorker = OneTimeWorkRequestBuilder<NotificationWorker>()
        .setInitialDelay(10, TimeUnit.SECONDS)
        .setConstraints(constraints)
        .build()

    workManager.enqueue(notificationWorker)

    //Monitor for state of work
    workManager.getWorkInfoByIdLiveData(notificationWorker.id)
        .observeForever { workInfo ->
            if (workInfo.state == WorkInfo.State.SUCCEEDED) {
                createSuccessNotification()
            } else {
                //createErrorNotification()
            }
        }
}



private fun setReminderByTime(reminder: Reminder) {
    val workManager = WorkManager.getInstance(Graph.appContext)
    val constraints = Constraints.Builder()
        .setRequiresBatteryNotLow(true)
        .build()

    val timeToEvent = reminder.reminderTime - Date().time

    val notificationWorker = OneTimeWorkRequestBuilder<NotificationWorker>()
        .setInitialDelay(timeToEvent, TimeUnit.MILLISECONDS)
        .setConstraints(constraints)
        .build()

    workManager.enqueue(notificationWorker)

    //Monitor for state of work
    workManager.getWorkInfoByIdLiveData(notificationWorker.id)
        .observeForever { workInfo ->
            if (workInfo.state == WorkInfo.State.SUCCEEDED) {
                createReminderMadeNotification(reminder)
            }
        }
}

@SuppressLint("NewApi")
private fun setReminderByLocation(reminder: Reminder) {
    val workManager = WorkManager.getInstance(Graph.appContext)
    val constraints = Constraints.Builder()
        .setRequiresBatteryNotLow(true)
        .build()

    //val timeToEvent = reminder.reminderTime - Date().time
    val locationData: Data = Data.Builder()
        .putDouble("latitude", reminder.locationLat!!)
        .putDouble("longitude", reminder.locationLng!!)
        .build()

    val notificationWorker = OneTimeWorkRequestBuilder<LocationNotificationWorker>()
        .setInputData(locationData)
        .setInitialDelay(5,TimeUnit.SECONDS)
        .setConstraints(constraints)
        .build()

    workManager.enqueue(notificationWorker)

    /*workManager.enqueueUniquePeriodicWork("uniikki", ExistingPeriodicWorkPolicy.REPLACE, notificationWorker)
    print("nyt laitettu tyoskentelemaa")
    //Monitor for state of work
    //workManager.getworkinfo
    workManager.getWorkInfosForUniqueWorkLiveData("uniikki")
        .observeForever { workInfo ->
            if (workInfo[0].state == WorkInfo.State.SUCCEEDED) {
                createReminderLocationNotification(reminder)
                workManager.cancelUniqueWork("uniikki")
            }
        }*/

    workManager.getWorkInfoByIdLiveData(notificationWorker.id)
        .observeForever { workInfo ->
            if (workInfo.state == WorkInfo.State.SUCCEEDED) {
                createReminderLocationNotification(reminder)
            } else {
                //repeat until success
                //workManager.enqueue(notificationWorker)
                //nicelyCursedRecursiveFun(workManager, notificationWorker, reminder)
                //well that didnt work either
            }
        }
}



private fun createNotificationChannel(context: Context) {
    // notification channel only for API 26+ because its new
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val name = "NotificationChannelName"
        val descriptionText = "NotificationChannelDescription"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel("CHANNEL_ID", name, importance).apply {
            description = descriptionText
        }
        // register the channel with the system
        val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}

private fun createSuccessNotification() {
    val notificationId = 1
    val builder = NotificationCompat.Builder(Graph.appContext, "CHANNEL_ID")
        .setSmallIcon(R.drawable.ic_launcher_background)
        .setContentTitle("Successsii")
        .setContentText("Nicettii")
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
    with(NotificationManagerCompat.from(Graph.appContext)) {
        //notificationId is unique for each notification
        notify(notificationId, builder.build())
    }
}

private fun createReminderMadeNotification(reminder: Reminder) {
    val notificationId = 3
    val builder = NotificationCompat.Builder(Graph.appContext, "CHANNEL_ID")
        .setSmallIcon(R.drawable.ic_launcher_background)
        .setContentTitle("Just reminding")
        .setContentText("${reminder.reminderMessage}, Date: ${reminder.reminderTime.toDateString()}")
        .setPriority(NotificationCompat.PRIORITY_HIGH)
    with(NotificationManagerCompat.from(Graph.appContext)) {
        notify(notificationId, builder.build())
    }
}

private fun createReminderLocationNotification(reminder: Reminder) {
    val notificationId = 5
    val builder = NotificationCompat.Builder(Graph.appContext, "CHANNEL_ID")
        .setSmallIcon(R.drawable.ic_launcher_background)
        .setContentTitle("Reached reminder location")
        .setContentText(reminder.reminderMessage)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
    with(NotificationManagerCompat.from(Graph.appContext)) {
        notify(notificationId, builder.build())
    }
}

data class ReminderViewState(
    val categories: List<Category> = emptyList()
)

private fun calculateDistance(){

}