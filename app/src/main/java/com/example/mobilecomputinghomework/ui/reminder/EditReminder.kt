package com.example.mobilecomputinghomework.ui.reminder

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.singleton.ReminderHolder
import com.example.mobilecomputinghomework.ui.home.categoryReminder.parseToTime
import com.example.mobilecomputinghomework.ui.home.categoryReminder.toDateString
import com.google.accompanist.insets.systemBarsPadding
import kotlinx.coroutines.launch
import java.util.*

@Composable
fun EditReminder(
    onBackPress: () -> Unit,
    viewModel: ReminderViewModel = viewModel()
) {
    val viewState by viewModel.state.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    val title = rememberSaveable{ mutableStateOf(ReminderHolder.readMessage()) }
    val date = rememberSaveable{ mutableStateOf(ReminderHolder.readDate().toDateString()) }
    var location = rememberSaveable{ mutableStateOf(ReminderHolder.readLocation()) }

    Surface() {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .systemBarsPadding()
        ) {
            TopAppBar {
                IconButton(
                    onClick = {
                        coroutineScope.launch {
                            viewModel.saveReminder(
                                com.example.mobilecomputinghomework.data.entity.Reminder(
                                    reminderMessage = ReminderHolder.readMessage(),
                                    reminderTime = ReminderHolder.readDate(),
                                    reminderCategoryId = getCategoryId(viewState.categories, "All"),
                                    locationLat = null,
                                    locationLng = null
                                )
                            )
                        }
                        onBackPress()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = null
                    )
                }
                Text(text = "Edit reminder")
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top,
                modifier = Modifier.padding(16.dp),
            ) {
                OutlinedTextField(
                    value = title.value,
                    onValueChange = { title.value = it },
                    label = { Text("Message")},
                    modifier = Modifier.fillMaxWidth(),
                )

                Spacer(modifier = Modifier.height(10.dp))
                OutlinedTextField(
                    value = date.value,
                    onValueChange = { date.value = it },
                    label = { Text("Reminder time")},
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number
                    )
                )
                Spacer(modifier = Modifier.height(20.dp))
                Button(
                    onClick = {
                        coroutineScope.launch {
                            viewModel.saveReminder(
                                com.example.mobilecomputinghomework.data.entity.Reminder(
                                    reminderMessage = title.value,
                                    reminderTime = date.value.parseToTime(),
                                    reminderCategoryId = getCategoryId(viewState.categories, "All"),
                                    locationLat = null,
                                    locationLng = null
                                )
                            )
                        }
                        onBackPress()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(55.dp),
                ) {
                    Text(text = "Save edits")
                }
            }
        }
    }
}

private fun getCategoryId(categories: List<Category>, categoryName: String): Long {
    return categories.first { category -> category.name == categoryName }.id
}