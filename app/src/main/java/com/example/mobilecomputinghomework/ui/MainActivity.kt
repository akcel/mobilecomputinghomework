package com.example.mobilecomputinghomework.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.app.ActivityCompat
import com.example.mobilecomputinghomework.singleton.LocationHolder
import com.example.mobilecomputinghomework.singleton.LocationHolder.writeLocation
import com.example.mobilecomputinghomework.singleton.ReminderHolder
import com.example.mobilecomputinghomework.singleton.SharedPref
import com.example.mobilecomputinghomework.ui.theme.MobileComputingHomeworkTheme
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task


class MainActivity : ComponentActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    @SuppressLint("MissingPermission") //ignore this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MobileComputingHomeworkTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MobileComputingApp()
                }
            }
        }
        SharedPref.init(getApplicationContext())
        ReminderHolder.init(getApplicationContext())
        LocationHolder.init(getApplicationContext())

        val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    // Permission is granted. Continue the action or workflow in your app.
                    fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location : Location? ->
                            updateLocation(location)
                        }

                    val locationRequest = LocationRequest.create()?.apply{
                        interval = 10000
                        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    }



                    val builder = LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest)

                    val client: SettingsClient = LocationServices.getSettingsClient(this)
                    val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
                    locationCallback = object : LocationCallback() {
                        override fun onLocationResult(p0: LocationResult) {
                            super.onLocationResult(p0)
                            //p0 ?: return
                            for (location in p0.locations){
                                // Update UI with location data
                                // ...
                                updateLocation(location)
                            }
                        }
                    }
                    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
                } else {
                    // Explain to the user that the feature is unavailable because the
                    // features requires a permission that the user has denied. At the
                    // same time, respect the user's decision. Don't link to system
                    // settings in an effort to convince the user to change their
                    // decision.
                }
            }
        requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
    }
}

fun updateLocation(location: Location?) {
    LocationHolder.writeLocation(location)
    //TODO täää pitäis olla varmasti viewModelissa?????
    //Varmaan koko homma pitäs
}