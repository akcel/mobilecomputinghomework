package com.example.mobilecomputinghomework.util

import android.content.Context
import android.os.SystemClock
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.mobilecomputinghomework.singleton.LocationHolder.readLocation
import kotlinx.coroutines.delay

class LocationNotificationWorker(
    context: Context,
    userParameters: WorkerParameters,
) : Worker(context, userParameters) {
    override fun doWork(): Result {
        //for (i in 0..10) {
        //    Log.i("LocationWorker", "Counted $i")
        //}
        val reminderLatitude = inputData.getDouble("latitude", 0.0)
        val reminderLongitude = inputData.getDouble("longitude", 0.0)

        while(!checkProximity(reminderLatitude,reminderLongitude)) {
            SystemClock.sleep(10000)
        }
        return Result.success()
    }

    private fun checkProximity(latitude: Double, longitude: Double) : Boolean {
        val distToNoti = 0.001 // about one block in lat=65
        val currentLocation = readLocation()

        if (currentLocation != null) {
            val currentLat = currentLocation.latitude
            val currentLng = currentLocation.longitude
            //logic for proximity
            if (latitude > currentLat - distToNoti &&
                latitude < currentLat + distToNoti &&
                longitude > currentLng - distToNoti &&
                longitude < currentLng + distToNoti
            ) {
                return true
            }
        }
        return false
    }
}

