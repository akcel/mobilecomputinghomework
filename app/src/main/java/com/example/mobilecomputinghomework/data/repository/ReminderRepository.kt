package com.example.mobilecomputinghomework.data.repository

import com.example.mobilecomputinghomework.data.entity.Reminder
import com.example.mobilecomputinghomework.data.room.ReminderDao
import com.example.mobilecomputinghomework.data.room.ReminderToCategory
import kotlinx.coroutines.flow.Flow

/**
 * A data repository for [Reminder] instances
 */
class ReminderRepository(
    private val reminderDao: ReminderDao
) {
    /**
     * Returns a flow containing the list of payments associasted with the category
     * with the given [categoryId]
     */
    fun remindersInCategory(categoryId: Long) : Flow<List<ReminderToCategory>> {
        return reminderDao.remindersFromCategory(categoryId)
    }

    /**
     * Add a new [Reminder] to the reminder store?
     */
    suspend fun addReminder(reminder: Reminder) = reminderDao.insert(reminder)

    suspend fun deleteReminder(reminder: Reminder) = reminderDao.delete(reminder)
}