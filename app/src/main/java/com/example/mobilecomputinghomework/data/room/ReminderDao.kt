package com.example.mobilecomputinghomework.data.room

import androidx.room.*
import com.example.mobilecomputinghomework.data.entity.Reminder
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ReminderDao {

    @Query(value = """
        SELECT reminders.* FROM reminders
        INNER JOIN categories ON reminders.reminder_category_id = categories.id
        WHERE reminder_category_id = :categoryId
    """)
    abstract fun remindersFromCategory(categoryId: Long): Flow<List<ReminderToCategory>>

    //abstract suspend fun getReminderWithName(name: String): Reminder?

    //@Query("SELECT * FROM categories WHERE id = :reminderId") TODO PALAUTA
    //abstract fun getReminderWithId(reminderId: Long): Reminder?

    //payment()..
    /*
    @Query("SELECT * FROM categories LIMIT 15")
    abstract fun categories(): Flow<List<Reminder>> /*suspend*/
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(entity: Reminder): Long
    //returns long (id in DB)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun update(entity: Reminder): Int

    @Delete
    abstract suspend fun delete(entity: Reminder): Int
    //Long is gonna be the id of the item deleted
}