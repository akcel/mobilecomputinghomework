package com.example.mobilecomputinghomework.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mobilecomputinghomework.data.entity.Category
import com.example.mobilecomputinghomework.data.entity.Reminder

/**
 * The [RoomDatabase] for this app
 */
@Database(
    entities = [Category::class, Reminder::class],
    version = 8,
    exportSchema = false
)
abstract class MobileComputingHomeworkDatabase : RoomDatabase(){
    abstract fun categoryDao(): CategoryDao
    abstract fun reminderDao(): ReminderDao
}