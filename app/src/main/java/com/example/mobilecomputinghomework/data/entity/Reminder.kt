package com.example.mobilecomputinghomework.data.entity

import androidx.room.*
import java.util.*

@Entity(
    tableName = "reminders",
    indices = [
        Index("id", unique = true),
        Index("reminder_category_id")
    ],
    foreignKeys = [
        ForeignKey(
            entity = Category::class,
            parentColumns = ["id"],
            childColumns = ["reminder_category_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Reminder(
    //Message, location_x, location_y, reminder_time, creation_time, creator_id, reminder_seen
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val reminderId: Long = 0,
    @ColumnInfo(name = "reminder_message") val reminderMessage: String,
    @ColumnInfo(name = "reminder_time") val reminderTime: Long,
    //@ColumnInfo(name = "creation_time") val creationTime: Long = Date().time,
    //@ColumnInfo(name = "creator_id") val creatorId: Long = 0,
    @ColumnInfo(name = "reminder_category_id") val reminderCategoryId: Long = 0,
    @ColumnInfo(name = "location_lat") val locationLat: Double?,
    @ColumnInfo(name = "location_lng") val locationLng: Double?,
    @ColumnInfo(name = "reminder_seen") val reminderSeen: Boolean = false,

    )
