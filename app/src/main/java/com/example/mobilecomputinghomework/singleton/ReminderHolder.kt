package com.example.mobilecomputinghomework.singleton

import android.content.Context
import java.util.*

object ReminderHolder {
    private var reminderMessage = "old message"
    private var reminderDate: Long = 0
    private var reminderLocation: String = ""

    fun init(context: Context) {

    }

    fun readMessage(): String {
        return reminderMessage;
    }

    fun readDate(): Long {
        return reminderDate;
    }

    fun readLocation(): String {
        return reminderLocation;
    }

    fun writeMessage(ireminderMessage:String) {
        reminderMessage = ireminderMessage
    }

    fun writeDate(ireminderDate:Long) {
        reminderDate = ireminderDate
    }

    fun writeLocation(ireminderLocation:String) {
        reminderLocation = ireminderLocation
    }
}