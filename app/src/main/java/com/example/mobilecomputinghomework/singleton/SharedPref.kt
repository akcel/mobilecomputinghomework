package com.example.mobilecomputinghomework.singleton

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences


// modified from https://stackoverflow.com/a/40446531
//SharedPref.write(SharedPref.USERNAME, "XXXX")
//String name = SharedPref.read(SharedPref.NAME, null)

object SharedPref {
    private var mSharedPref: SharedPreferences? = null
    const val USERNAME = "USERNAME"
    const val PASSWORD = "PASSWORD"
    fun init(context: Context) {
        if (mSharedPref == null) mSharedPref =
            context.getSharedPreferences(context.packageName, Activity.MODE_PRIVATE)

        //TODO remove these hardcoded credentials
        SharedPref.write(SharedPref.USERNAME, "test")
        SharedPref.write(SharedPref.PASSWORD, "1234")
    }

    fun read(key: String?, defValue: String?): String? {
        return mSharedPref!!.getString(key, defValue)
    }

    fun write(key: String?, value: String?) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putString(key, value)
        prefsEditor.commit()
    }
}